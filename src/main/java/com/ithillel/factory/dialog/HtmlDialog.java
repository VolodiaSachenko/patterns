package com.ithillel.factory.dialog;

import com.ithillel.factory.buttons.Button;
import com.ithillel.factory.buttons.HtmlButton;

public class HtmlDialog extends Dialog{
    public Button createButton() {
        return new HtmlButton();
    }
}
