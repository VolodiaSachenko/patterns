package com.ithillel.factory.dialog;

import com.ithillel.factory.buttons.Button;
import com.ithillel.factory.buttons.WindowButton;

public class WindowsDialog extends Dialog {
    public Button createButton() {
        return new WindowButton();
    }
}
