package com.ithillel.factory;

import com.ithillel.factory.dialog.Dialog;
import com.ithillel.factory.dialog.HtmlDialog;
import com.ithillel.factory.dialog.WindowsDialog;

public class Demo {
    private static Dialog dialog;

    public static void main(String[] args) {
        configure();
        runBusinessLogic();
    }

    static void configure() {
        if (System.getProperty("os.name").equals("Mac OS X")) {
            dialog = new WindowsDialog();
        } else {
            dialog = new HtmlDialog();
        }
    }

    static void runBusinessLogic() {
        dialog.renderWindow();
    }
}
