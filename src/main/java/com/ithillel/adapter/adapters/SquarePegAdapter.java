package com.ithillel.adapter.adapters;

import com.ithillel.adapter.round.RoundPeg;
import com.ithillel.adapter.square.SquarePeg;

public class SquarePegAdapter extends RoundPeg {
    private final SquarePeg peg;

    public SquarePegAdapter(SquarePeg peg) {
        this.peg = peg;
    }

    @Override
    public double getRadius() {
        return Math.sqrt(Math.pow(peg.getWidth() / 2, 2) * 2);
    }
}
